// Activity 16

/*
Create a for loop that will be initialized with the number provided by 
the user, will Stop when the value is less than or equal to O and will 
decrease by 1 every iteration.
*/
let num=Number(prompt("Input your number"));
console.log("The number you input is: " + num);



for (num1=num; num1 >= 0; num1--) {
	
	/*
	Create a condition that if the current value is less
	 than or equal to 50, stop the loop. 
	*/

	if (num1<=50) {
		
		console.log("The current value is at " + num1 + ". Terminating the loop");
		break;
	}

	/*
		Create another condition that if the current value is divisible by 10, 
		print a message that the number is being skipped and continue to 
		the next iteration of the loop. 

	*/

/*	console.log(num1);*/

	if (num1%10===0) {
		console.log("The number is divisible by 10. Skipping the Number.");

	} else {
		
		console.log(num1);
	}


	/*Create another condition that if the current value is divisible by 5, 
	print the number. */

	if (num1%5===0){
			console.log(num1);
	} 
	


}


/*
	Create a variable that will contain the string 
supercalifragilisticexpialidocious. 
*/

let string1="supercalifragilisticexpialidocious";

/*Create another variable that will store the consonants from the 
string. */
let string2=null;

for (let i = string1.length; i >= 0; i--) {
	
	if (string1[i]=='a' ||
		string1[i]=='e' ||
		string1[i]=='i' ||
		string1[i]=='o' ||
		string1[i]=='u' ) {

		continue;

	}	else{

		string2=string2+string1[i];
	}
}

console.log("The consonants are " + string2)
/*Create another for Loop that will iterate through the individual 
letters of the string based on its length. */


for (let x = string1.length; x >= 0; x--) {
	
	console.log(string1[x]);
}
/*Create an if statement that will check if the letter of the string is 
equal to a vowel and continue to the next iteration of the loop if it is 
true. 
*/

for (let y = string1.length; y >= 0; y--) {
	
	if (string1[y]=='a' ||
		string1[y]=='e' ||
		string1[y]=='i' ||
		string1[y]=='o' ||
		string1[y]=='u' ) {

		continue;

	}	else{

		/*Create an else statement that will add the letter to the second variable*/
		string2=string2+string1[y];

	}
}

	console.log("The consonants plus the string in instruction 12 are " + string2)




