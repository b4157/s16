// Repetition Control Structure

/* 3 types of looping instructions
	1. while
	2. do-while 
	3. for
*/

/*
	While Loop
	-Syntax
		while(expression/condition){
			statements;
		}
*/

// let count=5;

// while (count!==0){
// 	console.log("While: " + count);
// 	count--;
// }

// count=1;

// while (count!==6){
// 	console.log("While: " + count);
// 	count++;
// }

// // Do while
// /*
// 	Syntax:
// 		do{
// 			statement;
// 		}while();
// */

// let num1 = 1;

// do{
// 	console.log("Do-while: " + num1);
// 	num1++;
// }while(num1<6);

// console.log("Number in prompt")

// let num2 = Number(prompt("Give me a number"));

// do{
// 	console.log("Do While: " + num2);
// 	num2+=1;
// } while(num2<10);

// /* For loop

// 	Syntax:
// 		for(initialization; expression/condition, finalExpression){
// 			statement;
// 		}
// */

// console.log("For Loop");
// for(num3=1;num3<=5;num3++){
// 	console.log(num3);
// }


// /*Mini Activity
// 	For Loop
// 	Using for loop, display a series of numbers,
//  starting in 1, based on user input.
// */

console.log("Mini Activity")

let num = Number(prompt("Input a number:"));


for(num4=1;num4<=num;num4++){
	console.log(num4);
}

console.log("String Iteration");

let mystring = prompt("What is your name?");
console.log(mystring.length);

for (x=0; x<mystring.length; x++) {
	console.log(mystring[x]);
}

for(i=0; i<mystring.length; i++){
	if (
		mystring[i]=='a' || 
		mystring[i]=='e' || 
		mystring[i]=='i' || 
		mystring[i]=='o' || 
		mystring[i]=='u' 
		
		) {
			console.log(3);

	} else {
		console.log(mystring[i])
	}
}

/*
	Continue and Break Statements - Continue statement allows the code to go
	to the next iterati	on	
*/

for (count=0; count<=20; count++) {
	if(count%2===0){
		continue;
	}

	console.log("Continue and Break: " + count);

	if(count>10){
		break;
	}
}